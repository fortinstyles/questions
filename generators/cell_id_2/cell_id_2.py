#!/usr/bin/env python3
#
#  Monkey puzzle question with parity checking
#  Solution requires `if` condition
#

import os
from pathlib import Path
import random
import pandas as pd


def solutions(number, agg_num, agg, arith, L, parity):
    if parity == "even":
        listcomp = f"sum([(x {agg[1]} {arith[1]} {agg_num}) for x in {L} if x % 2 == 0])"
        forloop = f"R = []\nfor x in {L}:\n\tif x % 2 == 0:\n\t\tR.append( ( x {agg[1]} {arith[1]} {agg_num} ) / 2 )"
    else:
        listcomp = f"sum([(x {agg[1]} {arith[1]} {agg_num}) for x in {L} if x % 2 != 0])"
        forloop = f"R = []\nfor x in {L}:\n\tif x % 2 != 0:\n\t\tR.append( ( x {agg[1]} {arith[1]} {agg_num} ) / 2 )"
    return listcomp, forloop


random.seed(a=5303)

questions = pd.DataFrame()
answers = pd.DataFrame()
hints = pd.DataFrame()
alt_level_4 = pd.DataFrame()
alt_level_5 = pd.DataFrame()

agg_ops = {"square": "** 2", "cube": "** 3"}
arith_ops = {"plus": "+", "minus": "-", "multiplied by": "*", "divided by": "/"}
odd_even = ["odd", "even"]

for i in range(1, 101):
    number = random.randint(2, 101)
    agg_num = random.randint(2, 30)
    agg = random.choice(list(agg_ops.items()))
    arith = random.choice(list(arith_ops.items()))
    parity = random.choice(list(odd_even))
    L = range(1, number + 1)

    question = f"""
    {number} monkeys line up, each holding a sign.\n
    Each monkey's sign shows a positive number that is the {agg[0]} of her position in line {arith[0]} {agg_num}.\n
    What is the sum of the numbers held by monkeys in {parity} positions in line?"""

    listcomp, forloop = solutions(number, agg_num, agg, arith, L, parity)
    answer = str(int(eval(listcomp)))
    questions = questions.append({"Question": question}, ignore_index=True,)
    answers = answers.append({"Answer": answer}, ignore_index=True,)
    alt_level_4 = alt_level_4.append({"for-loop": forloop}, ignore_index=True,)
    alt_level_5 = alt_level_5.append({"list-comp": listcomp}, ignore_index=True,)

questions_csv = "cell_id_2.csv"
answers_csv = "cell_id_4.csv"
alt_level_4_csv = "cell_id_8.csv"
alt_level_5_csv = "cell_id_9.csv"
output_dir = Path("./exercises")
output_dir.mkdir(parents=True, exist_ok=True)

questions_path = output_dir / questions_csv
if questions_path.is_file():
    questions.to_csv(questions_path, mode="a", header=True)
else:
    questions.to_csv(questions_path)

answers_path = output_dir / answers_csv
if answers_path.is_file():
    answers.to_csv(answers_path, mode="a", header=True)
else:
    answers.to_csv(answers_path)

alt_level_4_path = output_dir / alt_level_4_csv
if alt_level_4_path.is_file():
    alt_level_4.to_csv(alt_level_4_path, mode="a", header=True)
else:
    alt_level_4.to_csv(alt_level_4_path)

alt_level_5_path = output_dir / alt_level_5_csv
if alt_level_5_path.is_file():
    alt_level_5.to_csv(alt_level_5_path, mode="a", header=True)
else:
    alt_level_5.to_csv(alt_level_5_path)
