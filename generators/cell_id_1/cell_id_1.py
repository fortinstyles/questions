#!/usr/bin/env python3
#
#  Monkey puzzle question without parity checking
#  No `if` condition required
#

import os
from pathlib import Path
import random
import numpy as np
import pandas as pd


def solutions(number, agg_num, agg, arith, L):
    listcomp = f"sum([(x {agg[1]} {arith[1]} {agg_num}) for x in {L}])"
    forloop = f"R = []\nfor x in {L}:\n\tR.append( ( x {agg[1]} {arith[1]} {agg_num} ) / 2 )"
    return listcomp, forloop


random.seed(a=5302)

questions = pd.DataFrame()
answers = pd.DataFrame()
hints = pd.DataFrame()
alt_level_3 = pd.DataFrame()
alt_level_4 = pd.DataFrame()

agg_ops = {"square": "** 2", "cube": "** 3"}
arith_ops = {"plus": "+", "minus": "-", "multiplied by": "*", "divided by": "/"}

for i in range(1, 101):
    number = random.randint(2, 101)
    agg_num = random.randint(2, 30)
    agg = random.choice(list(agg_ops.items()))
    arith = random.choice(list(arith_ops.items()))
    L = range(1, number + 1)

    question = f"""
    {number} monkeys line up, each holding a sign.\n
    Each monkey's sign shows a positive number that is the {agg[0]} of her position in line {arith[0]} {agg_num}.\n
    What is the sum of the numbers held by all the monkeys?"""

    listcomp, forloop = solutions(number, agg_num, agg, arith, L)
    answer = str(int(eval(listcomp)))
    questions = questions.append({"Question": question}, ignore_index=True,)
    answers = answers.append({"Answer": answer}, ignore_index=True,)
    alt_level_3 = alt_level_3.append({"for-loop": forloop}, ignore_index=True,)
    alt_level_4 = alt_level_4.append({"list-comp": listcomp}, ignore_index=True,)

questions_csv = "cell_id_1.csv"
answers_csv = "cell_id_3.csv"
alt_level_3_csv = "cell_id_4.csv"
alt_level_4_csv = "cell_id_5.csv"
output_dir = Path("./exercises")
output_dir.mkdir(parents=True, exist_ok=True)

questions_path = output_dir / questions_csv
if questions_path.is_file():
    questions.to_csv(questions_path, mode="a", header=True)
else:
    questions.to_csv(questions_path)

answers_path = output_dir / answers_csv
if answers_path.is_file():
    answers.to_csv(answers_path, mode="a", header=True)
else:
    answers.to_csv(answers_path)

alt_level_3_path = output_dir / alt_level_3_csv
if alt_level_3_path.is_file():
    alt_level_3.to_csv(alt_level_3_path, mode="a", header=True)
else:
    alt_level_3.to_csv(alt_level_3_path)

alt_level_4_path = output_dir / alt_level_4_csv
if alt_level_4_path.is_file():
    alt_level_4.to_csv(alt_level_4_path, mode="a", header=True)
else:
    alt_level_4.to_csv(alt_level_4_path)
